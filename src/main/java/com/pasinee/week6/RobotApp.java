package com.pasinee.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot body = new Robot("Body", 'B', 1, 0);
        Robot petter = new Robot("Petter", 'P', 10, 10);
        body.right();
        body.up();
        body.down();
        body.down();
        body.right();
        body.right();
        body.down();
        body.print();

        petter.down();
        petter.right();
        petter.down();
        petter.left();
        petter.print();

        for (int y = Robot.Y_MIN; y <= Robot.Y_MAX; y++) {
            for (int x = Robot.X_MIN; x <= Robot.X_MAX; x++) {
                if (body.getX() == x && body.getX() == y) {
                    System.out.print(body.getSymbol());
                } else if (petter.getX() == x && petter.getX() == y) {
                    System.out.print(petter.getSymbol());
                } else {
                    System.out.print("-");
                }

            }
            System.out.println();
        }


    }
}
