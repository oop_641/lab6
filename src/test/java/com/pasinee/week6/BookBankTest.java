package com.pasinee.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {

    @Test
    public void shouldDepositSuccess() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100.0);
    }

    @Test
    public void shouldDepositNagative() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(100);
        assertEquals(false, result);
        assertEquals(100.0);
    }

    @Test
    public void shouldWithdrawSuccess() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50);
    }

    @Test
    public void shouldWithdrawNagative() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100);
    }

    @Test
    public void shouldWithdrawOvergetBalance() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50);
    }

    @Test
    public void shouldWithdraw100Balande100() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0);
    }

    private void assertEquals(double d) {
    }

    private void assertEquals(boolean b, boolean result) {
    }

}
